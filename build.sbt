name := "Scala Tutorial"

description := """This is a walk around the oficial tutorials in http://docs.scala-lang.org/tutorials"""

version :=  "1.0"

scalaVersion := "2.11.1"

lazy val basics = project in file("basics")

lazy val futures_async = project in file("futures_async")

lazy val java_vs_scala = project in file("java_vs_scala")

lazy val root = project.in(file("."))
	.aggregate(basics, futures_async, java_vs_scala)

libraryDependencies += "org.scala-lang" % "scala-reflect" % "2.11.3"