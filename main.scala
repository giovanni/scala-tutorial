object Main extends App {
	println("""|**************************************************************************************************
			   |* Usage: 
			   |* 1. In SBT console, see the projects with the command 'projects'. 
			   |* 2. Select the project you wanna run with the command 'project name'.
			   |* 3. Now you can run the examples with command 'run'. 
			   |*
			   |* A list of main apps will appear select one to run
			   |**************************************************************************************************
		""".stripMargin)
}