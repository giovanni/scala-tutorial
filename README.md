Scala Lang Tutorial
=========

Utilize o codigo fonte aqui como ponto de partida para o estudo da linguagem Scala.

Os códigos fonte são baseados no tutorial oficial disponível em [http://docs.scala-lang.org/tutorials/tour/tour-of-scala.html](http://docs.scala-lang.org/tutorials/tour/tour-of-scala.html)

## Utilização

Utilize o [SBT](http://www.scala-sbt.org) para rodar o projeto

* Instale o SBT
* Entre na pasta do projeto
* Abra o console com o comando `sbt`. Será aberto um prompt interativo e com altocomplete usando tab
* Veja os projetos com digitando  `projects`
* Selecione um projeto digitando `project nome`. Exemplo `project basics`
* Rode os exemplos digitando `run`. Uma lista irá aparecer selecione um pelo número. *O SBT ordena aleatóriamente, então os exemplos não estarão na ordem do tutorial*. Siga o tutorial para saber qual rodar pelo nome.

Aconselho a ler o tutorial

License
----

MIT


**Free Software, Hell Yeah!**
