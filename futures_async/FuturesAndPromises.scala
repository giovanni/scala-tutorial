import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.util.{Success, Failure}

object FuturesAndPromises extends App with Example{
	val description = "This example search for the first ocurrence of a world in a file"
	
	override def execute(){
		println("Enter a keyworld")
		val keyword = Console.readLine
		
		val position = searchOcurrence("text.txt", keyword)
		println("searching...")
		position onComplete {
			case Success(v) => println(s"position is $v")
			case Failure(t) => println("Failure: " + t.getMessage)
		}
	}

	def searchOcurrence(file: String, keyWord: String): Future[Int] = future {
		val source  = scala.io.Source.fromFile("futures_async/text.txt")
		val index = source.toSeq.indexOfSlice(keyWord)
		source.close
		index
	}

	run

	
}