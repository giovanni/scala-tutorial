import scala.concurrent._
import scala.util.{Success, Failure}
import ExecutionContext.Implicits.global
import java.util.{Timer,TimerTask}
object SimpleBlockFuture extends App with Example {
	val description = "This code blocks the cpus and wait for the response"
	val started = System.currentTimeMillis
	override def execute(){
		
		val numProcessors = Runtime.getRuntime().availableProcessors()
		println(s"Number of processors $numProcessors")
		for (_ <- 1 to numProcessors) yield after2(1000, true)
		
		val later = after2(1000, true)
		later onComplete {
			case Success(value) => {
				val time = System.currentTimeMillis - started
				println(s"Later is completed with value $value and time is $time ms")
			}
			case Failure(t) => println("An error has occured: " + t.getMessage)
		}
	}
	def after1[T](time:Long, r: T): Future[T] = {
		Future {
			Thread.sleep(time)
			r
		}
	}

	def after2[T](delay: Long, value: T): Future[T] = {
		val promisse = Promise[T]()
		val timer = new Timer()
		timer.schedule(new TimerTask {
			def run(): Unit = promisse.success(value)
			}, delay)
		promisse.future
	}

	run
}