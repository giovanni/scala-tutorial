trait Example {
	def description: String
	def execute: Unit
	def run(){
		println(description)
		execute
	}
}