trait Buffer {
	type B
	val element: B
}
abstract class SeqBuffer extends Buffer {
	type U
	type B <: Seq[U]
	def length = element.length
}

abstract class IntSeqBuffer extends SeqBuffer {
	type U = Int
}

// Using type parameters
// 

abstract class Buffer2[+T] {
	val element: T
}
abstract class SeqBuffer2[U, +T <: Seq[U]] extends Buffer2[T]{
	def length = element.length
}

object AbstractType extends App {
	def newIntSeqBuf(elem: Int, elem2: Int) : IntSeqBuffer = {
		new IntSeqBuffer {
			type B = List[U]
			val element = List(elem, elem2)
		}
	} 

	def newIntSeqBuf2(elem: Int, elem2: Int): SeqBuffer2[Int, Seq[Int]] = {
		new SeqBuffer2[Int,List[Int]] {
			val element = List(elem, elem2)
		}
	}
	val buf = newIntSeqBuf(7,8)
	println(s"length = ${buf.length}")
	println(s"element = ${buf.element}")
	
	println("Now with type parameters")

	val buf2 = newIntSeqBuf2(4,5)
	println(s"length = ${buf2.length}")
	println(s"element = ${buf2.element}")

}