object SequenceComprehensions extends scala.App {
	
	def even(from: Int, to: Int): List[Int] = {
		for(i <- List.range(from, to) if i % 2 == 0) yield i
	}

	def pairNumbers(n: Int, v: Int) = 
		for(i <- 0 until n; j <-i until n if i + j == v) yield Pair(i, j)
	
	def pairNumbersSideEffects(n: Int, v: Int) {
		for(i <- Iterator.range(0, n);
			j <- Iterator.range(i, n) if i + j == v)
			println(s"($i, $j)")
	}

	println(even(0, 20))
	pairNumbers(20, 32) foreach {
		case (i, j) => println(s"($i, $j)")
	} 
	println("Now with sideEffects")
	pairNumbersSideEffects(30, 42)

}