object AnonymousFunction extends App {
	// Anonymous class. Equivalent to (x: Int) => x + 1
	// This function has type Int => Int shorhand for Function1[Int, Int]
	// 
	val func1 = new Function1[Int, Int] {
  		def apply(x: Int): Int = x + 1
	}
	// This function has type (Int, Int) => String shothand for Function2[Int, Int, String]
	// 
	val func2 = (x: Int, y: Int) => "(" + x + ", " + y + ")"
	// This function has type () => String shorthand for Function0[String]
	// 
	val func3 = () => { System.getProperty("user.dir") }

	def printFunc1(f: Int => Int) { println( f(2) ) }
	def printFunc2(f: (Int, Int) => String) { println( f(2,3) ) }
	def printFunc3(f: () => String) { println( f() ) }

	printFunc1(func1)
	printFunc2(func2)
	printFunc3(func3)

	// An annonymous function
	printFunc1((x) => x + 2)
	// OR
	printFunc1(x => x + 2)
	// OR
	printFunc1(_ + 2)
	printFunc2((x,y) => s"[$x - $y]")
	// OR
	printFunc2("[" + _ + "-" + _ +"]")
	printFunc3(()=> "This is a annonymous function")

}