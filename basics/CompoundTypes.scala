trait Cloneable extends java.lang.Cloneable {
	override def clone(): Cloneable = {
		super.clone().asInstanceOf[Cloneable]
	}
}
trait Resetable {
	def reset: Unit
}

class ImplementClass(val number:Int) extends Cloneable with Resetable {
	
	def reset(){
		println(s"reseting $number...")
	}
}

object CompoundTypes extends scala.App {
	def cloneAndReset(obj: Cloneable with Resetable) = {
		val cloned = obj.clone()
		obj.reset
		cloned
	}
	val obj = new ImplementClass(67)
	cloneAndReset(obj)

}