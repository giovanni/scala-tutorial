object PatternMatch extends App {
 def matchTest(x: Int): String = x match {
    case 0 => "zero"
    case 1 => "one"
    case 2 => "two"
    case a if a < 0 => "negative"
    case _ => "many"
  }
  println(matchTest(2))
  println(matchTest(-1))
  println(matchTest(3))

  def matchTest2(x: Any): Any = x match {
    case 1 => "one"
    case "two" => 2
    case y: Int => "scala.Int"
  }
  println(matchTest2("two"))
}