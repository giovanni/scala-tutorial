class Point(xc: Int, yc: Int) {
	var x: Int = xc
	var y: Int = yc
	private var description: Option[String] = None
	
	def this(x: Int, y: Int, description: Option[String]) {
		this(x, y)
		this.description = description
	}

	def move(dx: Int, dy: Int){
		x = x + dx
		y = y + dy
	}

	def +(that: Point){
		x += that.x
		y += that.y
	}
	// There is only four operations in this fashion: +, -, ! and ~
	def unary_+{
		x += 1
		y += 1
	}
	
	override def toString = if(description.isDefined) "(" + description.get + ": " + x + "," + y +")" else "("+ x + "," + y +")"
}

class ImutablePoint(val x: Int, val y: Int) {	

	def move(dx: Int, dy: Int) = new Point(dx + x, dy + y)
	def +(that: Point): Point = {
		new Point(x + that.x, y + that.y)
	}
	// There is only four operations in this fashion: +, -, ! and ~
	def unary_+ = {
		new Point(x + 1, y + 1)
	}
	override def toString = "(" + x + "," + y +")"
}

object Classes extends App {
	println("Mutable class")
	val pt = new Point(1,2)
	println(pt)
	pt.move(10, 10)
	println(pt)
	println("Immutable class")
	val pti = new ImutablePoint(3,4)
	println(pti)
	val newPt = pti.move(2,3)
	println(newPt)
	println(pti)

	val pointDescription = new Point(1, 2, Some("Description"))
	println("Using another constructor")
	println(pointDescription)

	println("Using + and unary_++ operations")

	pt + pointDescription
	//pt.+(pointDescription)
	println(pt)
	println(pointDescription)

	+pt
	println(pt)	
}