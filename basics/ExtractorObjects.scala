// To implement patter mathing with regular classes you will need implement companion objects
// This is based on http://danielwestheide.com/blog/2012/11/21/the-neophytes-guide-to-scala-part-1-extractors.html

trait User {
  def name: String
}

class FreeUser(val name: String) extends User
class PremiumUser(val name: String, val score: Int) extends User

object FreeUser {
  def apply(name:String) = new FreeUser(name)
  def unapply(user: FreeUser) = Some(user.name)
}
object PremiumUser {
  def unapply(user: PremiumUser) = Some((user.name, user.score))
}

object hightScore {
  private val threshold = 10
  val description = "A hightScore is a PremiumUser with score larger than $threshold"
  def unapply(user: PremiumUser): Boolean = user.score > threshold
}

object Twice {
  def apply(x: Int): Int = x * 2
  def unapply(z: Int): Option[Int] = if(z%2 == 0) Some(z/2) else None
}

object ExtractorObjects extends App {
  def printUser(user: User){
  	user match {
  		case FreeUser(name) => println(s"Hello $name you are a FreeUser")
  		case user @ hightScore() => println(s"Is a user with hightScore his name is: ${user.name}")
  		case PremiumUser(name, score) => println(s"Hello $name you are a PremiumUser your score is $score")
  	}
  }

  printUser(FreeUser("Vascovisks"))
  printUser(new PremiumUser("Gertrurdisks", 1))
  printUser(new PremiumUser("I'm the hight", 11))

  println("The extractor object can also extract arbitrary content")

  val x = 21

  x match {case Twice(n) => println(n);case _ => println("Not a twice")}

  val y = Twice(21)
  println(y)
  y match {case Twice(n)=>println(n)}

  val z = 22 
  z match {case Twice(n)=>println(n)}

}

