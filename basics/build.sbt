name := "Basics Tutorial"

description := """This is a walk around the oficial tutorials in http://docs.scala-lang.org/tutorials/tour"""

version :=  "1.0"

scalaVersion := "2.11.1"

libraryDependencies += "org.scala-lang" % "scala-reflect" % "2.11.3"