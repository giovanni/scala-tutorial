class MyBool(x: Boolean) {
  def and(that: MyBool): MyBool = if (x) that else this
  def or(that: MyBool): MyBool = if (x) this else that
  def negate: MyBool = new MyBool(!x)
  override def toString = x.toString
}
object MyBool {
	// MyBool(true) instend of new MyBool(true)
	def apply(x: Boolean) = new MyBool(x)
}

object Operators extends App {
	// Equivalent to
	//  
	// def not(x: MyBool) = x.negate; // semicolon required here
	// def xor(x: MyBool, y: MyBool) = x.or(y).and(x.and(y).negate)

	def not(x: MyBool) = x negate; // semicolon required here
	def xor(x: MyBool, y: MyBool) = (x or y) and not(x and y)	

	println(not(MyBool(true)))
	println(xor(MyBool(true), MyBool(false)))
}


