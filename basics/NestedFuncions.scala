object NestedFunctions extends App {
  import scala.annotation.tailrec
  def filter(xs: List[Int], threshold: Int) = {
    def process(ys: List[Int]): List[Int] =
      if (ys.isEmpty) ys
      else if (ys.head < threshold) ys.head :: process(ys.tail)
      else process(ys.tail)
    process(xs)
  }

  def fibonacci(number: Long): Long = {
  	@tailrec
  	def process(last: Long, current: Long, num: Long): Long = 
  		if(num == 0) current
  		else process(current, last + current, num - 1)
  	
  	if(number < 0) -1
  	else if(number == 0 || number == 1) 1
  	else process(1, 2, number - 2)
  }

  println(filter(List(1, 9, 2, 8, 3, 7, 4), 5))

  println(fibonacci(5))
}