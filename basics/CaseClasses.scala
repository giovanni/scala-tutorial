abstract class Term
case class Var(name: String) extends Term
case class Fun(arg: String, body: Term) extends Term
case class Appl(f: Term, v: Term) extends Term

object CaseClasses extends scala.App {
	// Constructor parameter of case classes are treated as public
	val x = Var("x")
	println(x.name)

	// The compiler implements equals, hashcode and toString for case classes
	val x1 = Var("x")
	val x2 = Var("x")
	val y1 = Var("y")
	println("" + x1 + " == " + x2 + " => " + (x1 == x2))
	println("" + x1 + " == " + y1 + " => " + (x1 == y1))

	// Case classes are good for pattern mathing
	// 
	println("Pattern Mathing")
	def printTerm(term: Term) {
	    term match {
	      case Var(n) =>
	        print(n)
	      case Fun(x, b) =>
	        print("^" + x + ".")
	        printTerm(b)
	      case Appl(f, v) =>
	        print("(")
	        printTerm(f)
	        print(" ")
	        printTerm(v)
	        print(")")
	    }
  }
  def isIdentityFun(term: Term): Boolean = term match {
    case Fun(x, Var(y)) if x == y => true
    case _ => false
  }
  val id = Fun("x", Var("x"))
  val id2 = Fun("y", Var("x"))
  val t = Fun("x", Fun("y", Appl(Var("x"), Var("y"))))
  printTerm(t)
  println
  println(isIdentityFun(id))
  println(isIdentityFun(id2))
  println(isIdentityFun(t))
}