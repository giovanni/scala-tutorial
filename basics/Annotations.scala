/**
* The reflections code is based on http://www.veebsbraindump.com/2013/01/reflecting-annotations-in-scala-2-10/
*/

class Source(val URL: String, val mail: String) extends scala.annotation.StaticAnnotation

@Source("http://google.com", "mail@email.com")
class MyClass {}

object Annotations extends App {
	import scala.reflect.runtime.universe._
	val myClassSymbol = typeOf[MyClass].typeSymbol.asClass
	val annotations = myClassSymbol.annotations
	val sourceAnnotation = typeOf[Source]
	val annotation = annotations.find(a => a.tpe == sourceAnnotation)
	annotation match {
		case Some(anno) => {
			println("Class is annotated with Source Annotation")
			println("Printing the list of literals for the annotation")
			val args = anno.scalaArgs
			args.foreach(a => println(showRaw(a))) 
			println("Getting the arguments of the annotation as values and printing")

			val annotationArgValues = args.map(a => a.productElement(0).asInstanceOf[Constant].value)
			val rMirror = runtimeMirror(getClass.getClassLoader)
			val classSymbol = sourceAnnotation.typeSymbol.asClass
			val classMirror = rMirror.reflectClass(classSymbol)
			val constructorMethodSymbol = sourceAnnotation.declaration(nme.CONSTRUCTOR).asMethod
			val constructorMethodMirror = classMirror.reflectConstructor(constructorMethodSymbol)
			val newInstance = constructorMethodMirror(annotationArgValues: _*).asInstanceOf[Source]
			println(s"URL: ${newInstance.URL} Mail: ${newInstance.mail}")
			println("I will try to get the url contents from internet")
			val result = scala.io.Source.fromURL(newInstance.URL)(scala.io.Codec("ISO-8859-1")).mkString
			println(result)
		}
		case None => println("Class DOESN't have annotation")
	}

}