object LocalTypeInference extends App {
  val x = 1 + 2 * 3         // the type of x is Int
  val y = x.toString()      // the type of y is String
  def succ(x: Int) = x + 1  // method succ returns Int values

  // Recursive need to define type
  def fac(n: Int): Int = if (n == 0) 1 else n * fac(n - 1)

  case class MyPair[A, B](x: A, y: B);

  def id[T](x: T) = x
  // Equivalent to 
  // val x: MyPair[Int, String] = MyPair[Int, String](1, "scala")
  // val y: Int = id[Int](1)
  val p = MyPair(1, "scala") // type: MyPair[Int, String]
  val q = id(1)              // type: Int

}