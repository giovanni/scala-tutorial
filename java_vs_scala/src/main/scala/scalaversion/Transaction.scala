package scalaversion

/**
 * @author Giovanni Silva
 *         Date: 10/28/14.
 */
case class Transaction(id: Int, orders: List[Order])
