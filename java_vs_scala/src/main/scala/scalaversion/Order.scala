package scalaversion

/**
 * @author Giovanni Silva
 *         Date: 10/28/14.
 */
case class Order(value: Float, id: Int)
