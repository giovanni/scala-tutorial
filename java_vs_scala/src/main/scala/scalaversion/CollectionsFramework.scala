package scalaversion

/**
 * @author Giovanni Silva
 *         Date: 10/28/14.
 */
object CollectionsFramework extends App {
   /**
    * Suppose we have a list of transactions, and we want to transform in a list of orders and sum all
    */
   val transactions = List(
     Transaction(1, List(Order(10.5f, 1), Order(31.6f, 2), Order(65.9f, 3))),
     Transaction(2, List(Order(12.6f, 4), Order(23.4f, 5), Order(15.14f, 6))),
     Transaction(3, List(Order(14.26f, 7), Order(45, 8), Order(79.3f, 9))),
     Transaction(4, List(Order(11.9f, 10), Order(11.9f,11), Order(43.2f, 12)))
   )
   println("Sum of all transactions")

   transactions.foreach({ t =>
     println(s"Transaction ${t.id} have sum of orders equal to ${sumOrder(t.orders)}" )
   })

  /**
   * Now suppose we want the sum of the sums of orders
   */
 //  val sumOfSums = transactions.map((t)=>t.orders.map(_.value).sum).sum
 //  val sumOfSums = transactions.map((t) => sumOrder(t.orders)).sum
   val sumOfSums = transactions.map(_.orders.map(_.value).sum).sum
   println(s"The sum of the sums equals $sumOfSums")

  /**
   * Lets do a little more complicated example
   * We will filter the orders that have a value > 15 and print the sum of then for all transactions
   */

   transactions.foreach({t =>
     println(s"Transaction ${t.id} have sum of > 15 equal to ${sumOrder(t.orders, Some((o) => o.value > 15))}")
   })

  // or

  transactions.foreach({t =>
    val sum = t.orders.filter(_.value > 15).map(_.value).sum
    println(s"Again: Transaction ${t.id} have sum of > 15 equal to $sum")

  })


   /**
    * Functions to sum the values of a list of orders
    * @param orders
    * @return
    */
   def sumOrder(orders: List[Order], filter: Option[Order => Boolean] = None): Float = {
     //orders.map((o)=> o.value).sum
     filter match {
       case Some(f) => orders.filter(f).map(_.value).sum
       case None => orders.map(_.value).sum
     }

   }

 }
