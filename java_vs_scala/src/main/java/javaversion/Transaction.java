package javaversion;

import java.util.List;

/**
 * @author Giovanni Silva
 *         Date: 10/28/14.
 */
public class Transaction {
    public Integer id;
    public List<Order> orders;

    public Transaction(Integer id, List<Order> orders) {
        this.id = id;
        this.orders = orders;
    }
}
