package javaversion;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * @author Giovanni Silva
 *         Date: 10/28/14.
 */
public class CollectionsFramework {
    public static void main(String[] args) {
        List<Transaction> transactions = Arrays.asList(
                new Transaction(1, Arrays.asList(new Order(10.5f), new Order(31.6f), new Order(65.9f))),
                new Transaction(2, Arrays.asList(new Order(12.6f), new Order(23.4f), new Order(15.14f))),
                new Transaction(3, Arrays.asList(new Order(14.26f), new Order(45f), new Order(79.3f))),
                new Transaction(4, Arrays.asList(new Order(11.9f), new Order(13f), new Order(43.2f)))
        );
        System.out.println("Sum of all Transactions");
        for(int i =0;i<transactions.size();i++){
            Transaction t = transactions.get(i);
            Float sum = sumOfOrder(t.orders);
            System.out.println("Transaction " + t.id + " have sum of orders equal to " + sum);
        }

        Float sumOfSums = 0f;

        for(Transaction t : transactions){
            Float sum = sumOfOrder(t.orders);
            sumOfSums += sum;
        }
        System.out.println("Sum of sums: " + sumOfSums);

        for(int i =0;i<transactions.size();i++){
            Transaction t = transactions.get(i);
            Float sum = sumOfOrderWithValueMoreThen15(t.orders);
            System.out.println("Transaction " + t.id + " have sum of orders > 15 equal to " + sum);
        }
        // Or with a predicate

        for(int i =0;i<transactions.size();i++){
            Transaction t = transactions.get(i);
            Float sum = sumOfOrderPredicate(t.orders, new Predicate() {
                @Override
                public boolean test(Object o) {
                    return ((Order) o).value > 15;
                }
            });
            System.out.println("Transaction " + t.id + " have sum of orders > 15 equal to " + sum);
        }

        // Or now in java 8 with lambdas expression
        for(int i =0;i<transactions.size();i++){
            Transaction t = transactions.get(i);
            Float sum = sumOfOrderPredicate(t.orders, x -> ((Order) x).value > 15);
            System.out.println("Using Lambda Expression -: Transaction " + t.id + " have sum of orders > 15 equal to " + sum);
        }

        // Now a Java 8 Idiomatic

        transactions.forEach((t) -> {
                System.out.println("Using Java 8 Loops and functions -: Transaction " + t.id
                        +" have sum of orders > 15 equal to "
                        + sumWithJava8(t.orders, Optional.of(x -> ((Order) x).value > 15)));
        });


    }

    public static Float sumOfOrder(List<Order> orders){
        Float sum = 0f;
        for(Order order : orders){
            sum += order.value;
        }
        return sum;
    }

    public static Float sumOfOrderWithValueMoreThen15(List<Order> orders){
        Float sum = 0f;
        for(Order order : orders){
            if(order.value > 15)
                sum += order.value;
        }
        return sum;
    }

    public static Float sumOfOrderPredicate(List<Order> orders, Predicate predicate) {
        Float sum = 0f;
        for(Order order : orders){
            if(predicate.test(order))
                sum += order.value;
        }
        return sum;
    }

    // A Java 8 Idiomatic

    public static Float sumWithJava8(List<Order> orders, Optional<Predicate<Order>> filter) {
        Float retorno = 0f;
        if(filter.isPresent()){
            retorno = orders.stream()
                    .filter(filter.get())
                    .map( o ->  o.value)
                    .reduce(0f, (accumulator, _item) -> accumulator + _item);
        }else{
           retorno = orders.stream()
                   .map(o -> o.value)
                   .reduce(0f, (accumulator, _item) -> accumulator + _item);
        }
        return retorno;

    }

}
