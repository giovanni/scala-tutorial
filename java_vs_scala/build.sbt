name := "Java vs Scala"

description :=
  """This project is a compilation of problems solved by Java and Scala
    |It focus in introduce scala language comparing to a imperative Java and also a more functional Java 8
  """.stripMargin

version :=  "1.0"

scalaVersion := "2.11.1"

libraryDependencies += "com.google.guava" % "guava" % "18.0"